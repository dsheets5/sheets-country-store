// Rotates icons every 1.5 seconds
function rotateFile(){
    let file = document.getElementById('phone');
    file.innerHTML = "&#xf2a0"

    setTimeout(function(){
        file.innerHTML = "&#xf0a4"
    }, 1500)

    

}

rotateFile();
setInterval(rotateFile, 3000)




function myMap() {
    var mapProp= {
      center:new google.maps.LatLng(36.394496,-81.406561),
      zoom:16,
    };
    var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

    var marker = new google.maps.Marker({position: new google.maps.LatLng(36.394496,-81.406561), map: map});
    marker.setMap(map);
}


const first = document.querySelector('.slide');
const slide = () => {
  const before = document.querySelector('.showing');
  if (before) {
    before
      .classList
      .remove('showing');
    const next = before.nextElementSibling;
    if (next) {
      next
        .classList
        .add('showing')
    } else {
      first
        .classList
        .add('showing');
    }
  } else {
    first
      .classList
      .add('showing');
  }
}
slide();
setInterval(slide, 3500);





//Typewriter class

class TypeWriter {
    constructor(txtElement, words, wait = 3000) {
    this.txtElement = txtElement;
    this.words = words;
    this.txt = '';
    this.wordIndex = 0;
    this.wait = parseInt(wait, 10);
    this.type();
    this.isDeleting = false;
    }
    
    type() {
    // Current index of word
    const current = this.wordIndex % this.words.length;
    // Get full text of current word
    const fullTxt = this.words[current];
    
    // Check if deleting
    if(this.isDeleting) {
    // Remove char
    this.txt = fullTxt.substring(0, this.txt.length - 1);
    } else {
    // Add char
    this.txt = fullTxt.substring(0, this.txt.length + 1);
    }
    
    // Insert txt into element
    this.txtElement.innerHTML = `<span class="txt">${this.txt}</span>`;
    
    // Initial Type Speed
    let typeSpeed = 300;
    
    if(this.isDeleting) {
    typeSpeed /= 2;
    }
    
    // If word is complete
    if(!this.isDeleting && this.txt === fullTxt) {
    // Make pause at end
    typeSpeed = this.wait;
    // Set delete to true
    this.isDeleting = true;
    } else if(this.isDeleting && this.txt === '') {
    this.isDeleting = false;
    // Move to next word
    this.wordIndex++;
    // Pause before start typing
    typeSpeed = 500;
    }
    
    setTimeout(() => this.type(), typeSpeed);
    }
    }
    
    
    // Init On DOM Load
    document.addEventListener('DOMContentLoaded', init);
    
    // Init App
    function init() {
    const txtElement = document.querySelector('.txt-type');
    const words = JSON.parse(txtElement.getAttribute('data-words'));
    const wait = txtElement.getAttribute('data-wait');
    // Init TypeWriter
    new TypeWriter(txtElement, words, wait);
    }






